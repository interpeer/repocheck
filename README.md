# repocheck

Scripts for maintaining a repo, e.g. ensure the correct files are present and some content is as expected, etc.

This is mainly for internal use, made 0BSD because why not?

1. Go to your working directory.
1. Run `repocheck.py`

**Warning:** this is all very hacky.

## Templates

Files in templates will be installed into you repo.

- As-is, if the target is not present.
- With the `.new` extension if the target is present.

If the template ends in `.in`, this extension is stripped. The file content is
then passed through [Jinja2](https://jinja.palletsprojects.com/) for templating.
