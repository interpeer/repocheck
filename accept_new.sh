#!/bin/bash

for fname in $(find . -type f -name '*.new') ; do
  target=$(echo $fname | sed 's/\.new//g')
  mv "$fname" "$target"
done
