#!/usr/bin/env python3
#
# Copyright (C) 2022 Interpeer gUG (haftungsbeschraenkt)
#
# SPDX-License-Identifier: 0BSD
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

import re

COPYRIGHT_PATTERN = re.compile(r'copyright.*', re.IGNORECASE | re.MULTILINE)
COPYRIGHT_YEAR_PATTERN = re.compile(r'copyright *\(c\)* (?P<years>[0-9-]+).*',
        re.IGNORECASE | re.MULTILINE)

GPLv3_TEXT = """SPDX-License-Identifier: GPL-3.0-only

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."""

LICENSE_PATTERN_LINE_START = r'^ *\*? *'
LICENSE_PATTERN_LINE_END = r' *$'

def headerize_string(license):
    ret = r''
    for line in license.splitlines():
        if len(line):
            ret += r' * ' + line + '\n'
        else:
            ret += r' *\n'
    return ret

def patternize_string(license):
    ret = r''
    for line in license.splitlines():
        normalized = line.replace('(', '\\(')
        normalized = normalized.replace(')', '\\)')
        normalized = normalized.replace('.', '\\.')
        normalized = normalized.replace('^', '\\^')
        normalized = normalized.replace('$', '\\$')
        if len(normalized):
            ret += LICENSE_PATTERN_LINE_START + normalized + LICENSE_PATTERN_LINE_END + '\n'
        else:
            ret += LICENSE_PATTERN_LINE_START + '$\n'
    return ret

GPLv3_PATTERN = re.compile(patternize_string(GPLv3_TEXT), re.IGNORECASE | re.MULTILINE)
GPLv3_HEADER = headerize_string(GPLv3_TEXT)
GPLv3_LINK = 'https://spdx.org/licenses/GPL-3.0'

LICENSE_PATTERN = GPLv3_PATTERN
LICENSE_HEADER = GPLv3_HEADER
LICENSE_LINK = GPLv3_LINK

LICENSE_PATTERN_GENERIC = re.compile(r"""^(?P<preamble>(.*\r?\n)*?)(?P<copyright>( *\*? *Copyright.*\r?\n)+)(.*\r?\n)*?(?P<code> *\*+/\r?\n(.*\r?\n)*)""",
#(?P<code>^ *\*?$.*)""",
    re.IGNORECASE | re.MULTILINE)

VERBOSE=True

def dlog(message):
    if VERBOSE:
        import sys
        sys.stderr.write('>>> ' + message + '\n')


def run_subprocess(command, error):
    import subprocess
    ret = subprocess.run(command, capture_output = True)
    assert ret.returncode == 0, "%s: %s" % (error, ret.stderr)

    blines = ret.stdout.split(b'\n')
    lines = []
    for bline in blines:
        if len(bline) <= 0:
            continue

        try:
            fname = bline.decode('utf8')
            lines.append(fname)
        except UnicodeDecodeError as ex:
            import sys
            sys.stderr.write('%s: %s\n' % (bline, ex))
    return lines



def get_file_list():
    return run_subprocess(['git', 'ls-tree', '--full-tree', '-r', '--name-only', 'HEAD'],
        'Could not get file list from git')

def git_rename(old, new):
    return run_subprocess(['git', 'mv', old, new],
        'Could not rename alias to required file')


def get_file_contents(filename):
    try:
        with open(filename, 'rb') as fh:
             bcontents = fh.read()
    except IsADirectoryError as ex:
        import sys
        sys.stderr.write('Get file contents of %s: %s\n' % (filename, ex))
        return None

    try:
        contents = bcontents.decode('utf8')
        return contents
    except UnicodeDecodeError as ex:
        import sys
        sys.stderr.write('Decode file contents of %s: %s\n' % (filename, ex))
        return None


def get_copyright_mentions(filename, contents):
    matches = COPYRIGHT_PATTERN.findall(contents)
    return matches

def get_license_mentions(filename, contents):
    matches = LICENSE_PATTERN.findall(contents)
    return matches

def replace_license_generic(filename, contents):
#    matches = LICENSE_PATTERN_GENERIC.search(contents)
#    if matches is None:
#        print('nope', filename)
#        return
#
#    print(filename, matches.groupdict())
    res = LICENSE_PATTERN_GENERIC.sub('\g<preamble>\g<copyright> *\n' + LICENSE_HEADER + '\g<code>',
            contents)
    if res == contents:
        return False

    new_filename = filename + '.new'
    with open(new_filename, 'w') as fh:
        fh.write(res)
    dlog(filename + ' updated to ' + new_filename)

    return True

def this_year():
    import datetime
    now = datetime.datetime.now()
    return now.date().strftime('%Y')


def gaps_found(candidates, contents):
    # We parse the year ranges in all lines here, and try to assemble a
    # seamless listory
    for candidate in candidates:
        ret = COPYRIGHT_YEAR_PATTERN.findall(contents)
        years = set()
        for yeargroup in ret:
            if '-' in yeargroup:
                start, end = yeargroup.split('-')
                r = range(int(start.strip()), int(end.strip()) + 1)
                for y in r:
                    years.add(y)
            else:
                years.add(int(yeargroup.strip()))
        if len(years) <= 0:
            continue

        last = None
        for year in sorted(years):
            if last is None:
                last = year
                continue
            if year != last + 1:
                # print('GAP DETECED?', year, last)
                # print(sorted(years), 'from', candidates)
                return True
            last = year
    return False


def all_searches_found(to_search, candidates):
    # We need to find all to search for things in a single candidate.
    for candidate in candidates:
        found = 0
        for pat in to_search:
            if candidate.find(pat) >= 0:
                found += 1
        if found == len(to_search):
            return True
    return False


def perform_file_checks(files):
    # Get list of required files
    import sys, os.path
    dirname = os.path.dirname(sys.argv[0])
    listname = os.path.join(dirname, 'filelist.txt')

    checks = []
    with open(listname, 'r') as fh:
        for line in fh.readlines():
            line = line.strip()
            names = line.split(':')
            checks.append({
                'required': names[0],
                'aliases': names[1:],
            })

    renamed = {}
    # Check for files
    for required in checks:
        # If it's already in thre file list, we're ok.
        if required['required'] in files:
            continue

        # If not and we can stat it, we're also ok - maybe it's been manually
        # added.
        if os.path.isfile(required['required']):
            renamed[required['required']] = required['aliases']
            continue

        found = False
        for alias in required['aliases']:
            if alias in files:
                found = True
                dlog('Did not find required file %s, but found '
                    'alias %s\n' % (required['required'], alias))
                git_rename(alias, required['required'])
                renamed[required['required']] = [alias]
                break
        if not found:
            print(required['required'])

    return renamed


def check_and_update_license_info(files, to_search):
    for filename in files:
        contents = get_file_contents(filename)
        if contents is None:
            continue

        mentions = get_copyright_mentions(filename, contents)
        # Ignore files without copyright mention
        if len(mentions) == 0:
            continue

        license_matches = get_license_mentions(filename, contents)
        if len(license_matches) == 0:
            dlog('Bad copyright header in %s' % (filename,))
            if not replace_license_generic(filename, contents):
                print(filename)
                continue

        # Check there are no gaps in the history
        if gaps_found(mentions, contents):
            dlog('Gaps found in history in %s' % (filename,))
            print(filename)
            continue

        # Make sure all search patterns are found
        if not all_searches_found(to_search, mentions):
            dlog('Did not find all search patterns in %s' % (filename,))
            print(filename)

def write_output(filename, contents):
    target = filename
    import os.path
    if os.path.isfile(target):
        existing = get_file_contents(target)
        if existing == contents:
            return None

        target = target + '.new'

    with open(target, 'w') as fh:
        fh.write(contents)

    return target


def ensure_template_entry(ctx, entry, target, files):
    import os, os.path
    if os.path.isdir(entry):
        dlog('Ensuring existence of directory: %s' % (target,))
        os.makedirs(target, exist_ok=True)
        print(target + os.path.sep)
        return

    contents = get_file_contents(entry)

    if target.endswith('.in'):
        realtarget = target[:-3]
        dlog('Creating from template: %s' % (target,))

        template = ctx['env'].get_template(target)
        result = template.render(ctx['vars'])
        if result[-1] != '\n':
            result += '\n'

        written = write_output(realtarget, result)
        if written is not None:
            print(written)
    else:
        realtarget = target
        dlog('Creating a copy: %s' % (target,))

        written = write_output(realtarget, contents)
        if written is not None:
            print(written)

def get_template_context(templates):
    from jinja2 import Environment, FileSystemLoader, select_autoescape
    env = Environment(
        loader=FileSystemLoader(templates),
        autoescape=select_autoescape()
    )
    ctx = {
        'env': env,
        'vars': {},
    }

    # Repo name
    res = run_subprocess(['git', 'rev-parse', '--show-toplevel'],
        'Could not get repo name')
    import os.path
    ctx['vars']['REPO_NAME'] = os.path.basename(res[0])

    # License link
    ctx['vars']['LICENSE_LINK'] = LICENSE_LINK

    # First commit
    res = run_subprocess(['git', 'rev-list', '--max-parents=0', 'HEAD', '--date=iso8601', '--pretty'],
        'Could not get first repo commit')
    for l in res:
        if l.startswith('Date:'):
            date = l[5:].strip()
            ctx['vars']['FIRST_COMMIT_DATE'] = date
            break

    return ctx


def generate_from_templates(files):
    # Get template files
    import sys, os.path, os
    dirname = os.path.dirname(sys.argv[0])
    templates = os.path.join(dirname, 'templates')

    # Create template context
    ctx = get_template_context(templates)

    # Process
    import glob
    for entry in glob.iglob(os.path.join(templates, '**'), recursive=True):
        target = entry[len(templates) + 1:]
        if not len(target):
            continue

        ensure_template_entry(ctx, entry, target, files)



if __name__ == '__main__':
    files = get_file_list()

    import sys
    to_search = []
    if len(sys.argv) > 1:
        to_search = sys.argv[1:]
    else:
        to_search = ['Interpeer gUG (haftungsbeschränkt)', this_year()]

    # Perform file checks
    renamed = perform_file_checks(files)

    # Update files from renaming, etc.
    for newname, aliases in renamed.items():
        for alias in aliases:
            if alias in files:
                files.remove(alias)
        files.append(newname)

    # Check that files from templates are up-to-date and/or exist
    added = generate_from_templates(files)

    # Licensing stuff
    check_and_update_license_info(files, to_search)
